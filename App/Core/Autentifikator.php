<?php

namespace App\Core;

class Autentifikator
{
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    //prihlásenie
    function loginUser($kto) {
        $_SESSION["user"] = $kto;
    }

    //odhlásenie
    function logoutUser() {
        if (isset($_SESSION["user"])) {
            unset($_SESSION["user"]);
            session_destroy();
        }
    }

    //admin?
    function isAdmin() {
        $admin = "admin@gmail.com";
        if (($this->isLoggedIn() == 1) && ($admin == $this->loggedInUser()->getEmail())) {
            return 1;
        } else {
            return 0;
        }
    }

    //je niekto prihlásený
    function isLoggedIn() {
        if (isset($_SESSION["user"])) {
            return 1;
        } else {
            return 0;
        }
    }

    //prihlásený používateľ
    function loggedInUser() {
        return $_SESSION["user"];
    }


}

