<div class="container-gallery">
    <h1>Galéria našich zážitkov</h1>
    <p>Pozrite sa s nami na krásy celého sveta a vychutnajte si tieto dychberúce zábery.</p>
</div>

<div class="container gallery-container">
    <div class="tz-gallery">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/a.jpg">
                    <img src="public/img/a.jpg" alt="Sky">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/b.jpg">
                    <img src="public/img/b.jpg" alt="Sky">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/b2.jpg">
                    <img src="public/img/b2.jpg" alt="Sky">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/b3.jpg">
                    <img src="public/img/b3.jpg" alt="India">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/waterfall.jpg">
                    <img src="public/img/waterfall.jpg" alt="India">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/bar.jpg">
                    <img src="public/img/bar.jpg" alt="Paríž">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/bb.jpg">
                    <img src="public/img/bb.jpg" alt="Benátky">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/ch.jpg">
                    <img src="public/img/ch.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/d1.jpg">
                    <img src="public/img/d1.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/louvre.jpg">
                    <img src="public/img/louvre.jpg" alt="India">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/fishing.jpg">
                    <img src="public/img/fishing.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/lon.jpg">
                    <img src="public/img/lon.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/ne.jpg">
                    <img src="public/img/ne.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/p.jpg">
                    <img src="public/img/p.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/paris.jpg">
                    <img src="public/img/paris.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/q.jpg">
                    <img src="public/img/q.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/rom.jpg">
                    <img src="public/img/rom.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/sanfran.jpg">
                    <img src="public/img/sanfran.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/t.jpg">
                    <img src="public/img/t.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/venice.jpg">
                    <img src="public/img/venice.jpg" alt="NY">
                </a>
            </div>
            <div class="col-sm-6 col-md-4">
                <a class="lightbox" href="public/img/wa.jpg">
                    <img src="public/img/wa.jpg" alt="NY">
                </a>
            </div>

        </div>

    </div>

</div>
