<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="public/css/styl.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Kontakt</title>
</head>
<div class="container-gallery">
    <h1>Kontakt</h1>
    <p>Napíšte nám a dozviete sa viac!</p>
</div>

<div class="wrapper-contact">
    <div>
        <ul>
            <li>
                <a href="tel:+421915156211"><i class="fa fa-phone" aria-hidden="true"></i> +421 915 156 211</a>
            </li>
            <li>
                <a href="mailto:travelwithus@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i> travelwithus@gmail.com</a>
            </li>
            <li>
                <a href=""><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>
            </li>
        </ul>
    </div>

    <div>
    <div class="map-wrapper">
        <iframe class="googlemap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d460321.7667850385!2d21.213195001371044!3d48.99972122315265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473eed9e750e95b3%3A0x98efaa84dccd26c!2zUHJlxaFvdg!5e0!3m2!1ssk!2ssk!4v1610924752626!5m2!1ssk!2ssk" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    </div>



</div>
</html>
