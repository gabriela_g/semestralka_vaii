<div class="container-gallery">
    <h1>Kvíz</h1>
    <p>Over si svoje vedomosti v jednoduchom kvíze!</p>
</div>

<table >
    <tr>
        <div >
            <p class="question">1. Hlavný mestom Poľska je...</p>
                <ul><br>
                    <input type="radio" class="answer"  name="q1" value="1">
                    <label id="correctString1">Varšava</label><br>
                    <input class="answer" type="radio" name="q1" value="0">
                    <label>Gdansk</label> <br>
                    <input class="answer" type="radio" name="q1" value="0">
                    <label>Lodž</label><br>
                    <input class="answer" type="radio" name="q1" value="0">
                    <label>Krakov</label><br>
                </ul>
        <br>
        </div>
    </tr>
    <tr>
            <div>
                <p class="question">2. Hlavným mestom Chorvátska je...</p>

                <ul><br>
                    <input class="answer" type="radio" name="q2" value="0">
                    <label>Dubrovnik</label><br>

                    <input class="answer" type="radio" name="q2" value="1">
                    <label id="correctString2">Záhreb</label><br>

                    <input class="answer" type="radio" name="q2" value="0">
                    <label>Split</label><br>

                    <input class="answer" type="radio" name="q2" value="0">
                    <label>Zadar</label><br>
                </ul>
                <br>
            </div>
    </tr>
    <tr>
            <div>
                <p class="question">3. Hlavným mestom Holandska je...</p>

                <ul><br>
                    <input class="answer" type="radio" name="q3" value="0">
                    <label>Bern</label> <br>
                    <input class="answer" type="radio" name="q3" value="1">
                    <label id="correctString3">Amsterdam</label> <br>
                    <input class="answer" type="radio" name="q3" value="0">
                    <label>Brusel</label><br>
                    <input class="answer" type="radio" name="q3" value="0">
                    <label>Sofia</label><br>
                </ul>
                <br>
            </div>
    </tr>
    <tr>
            <div>
                <p class="question">4. Hlavným mestom Grécka je...</p>

                <ul><br>
                    <input class="answer" type="radio" name="q4" value="0">
                    <label>Babylon</label>  <br>
                    <input class="answer" type="radio" name="q4" value="0">
                    <label>Kyjev</label>  <br>
                    <input class="answer" type="radio" name="q4" value="0">
                    <label>Rím</label>  <br>
                    <input class="answer" type="radio" name="q4" value="1">
                    <label id="correctString4">Atény</label><br>
                </ul>
                <br>
            </div>
    </tr>
</table>
<br>
    <div>
<div class="submitter">
    <input class="quizSubmit" id="submitButton" onClick="submitQuiz()" type="submit" value="Potvrdiť" />
</div>
    </div>
<!--show only wrong answers on submit-->
<div class="quizAnswers" id="correctAnswer1"></div>
<div class="quizAnswers" id="correctAnswer2"></div>
<div class="quizAnswers" id="correctAnswer3"></div>
<div class="quizAnswers" id="correctAnswer4"></div>

<!--show score upon submit-->
<div>
    <h2 class="quizScore" id="userScore"></h2>
</div>

