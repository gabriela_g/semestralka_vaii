<?php /** @var Array $data */
/** @var App\Core\Autentifikator $autentifikacia */
$autentifikacia = new App\Core\Autentifikator();
?>
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css"
      rel="stylesheet"  type='text/css'>

<div id="test">
<div id="container-bg">
    <div class="content-wrapper">
        <?php if ($autentifikacia->isLoggedIn() == 0) { ?>
            <h1>Travel with us</h1>
            <p>Cestujte s nami po celom svete</p>
            <a href="?c=Login">Prihlásiť</a>
        <?php } else if ($autentifikacia->isAdmin() == 1) { ?>
            <h1>Ahoj <?= $autentifikacia->loggedInUser()->getNickname() ?> ! </h1>
            <p>Pozri si používateľov a ich pripomienky</p>
            <a href="?c=Login&a=zobraz">Pripomienky</a>
        <?php } else { ?>
            <h1>Ahoj <?= $autentifikacia->loggedInUser()->getNickname() ?> ! </h1>
            <p>Vytvor spolu s ostatnými galériu fotiek</p>
            <a href="?c=Login&a=galleryUser">Galéria</a>
        <?php } ?>

    </div>
    <div class="btn-down">
        <a href="#introduction-section">
            <i class="fa fa-angle-double-down" aria-hidden="true"></i>
        </a>
    </div>
</div>
    </div>

<div id="introduction-section">
    <h2>Naše služby</h2>
    <hr>
</div>

<div id="grid-layout-33">
    <div>
        <i class="fa fa-suitcase" aria-hidden="true"></i>
        <h3>Služobné cesty</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi aspernatur debitis eligendi ex fugiat, hic magnam maiores, molestiae nam nemo officiis quis, quod repellat rerum sint temporibus velit voluptates.</p>
    </div>
    <div>
        <i class="fa fa-sun-o" aria-hidden="true"></i>
        <h3>Dovolenky</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi aspernatur debitis eligendi ex fugiat, hic magnam maiores, molestiae nam nemo officiis quis, quod repellat rerum sint temporibus velit voluptates.</p>
    </div>
    <div>
        <i class="fa fa-map-o" aria-hidden="true"></i>
        <h3>Výlety</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus animi aspernatur debitis eligendi ex fugiat, hic magnam maiores, molestiae nam nemo officiis quis, quod repellat rerum sint temporibus velit voluptates.</p>
    </div>
</div>


<div id="test">
    <div id="content-video">
        <h2>Čo u nás môžete zažiť?</h2>
        <hr>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/vwZBiG1_IWs" frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

