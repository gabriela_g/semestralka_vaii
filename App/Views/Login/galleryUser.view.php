<?php
$conn = new mysqli("localhost","root","dtb456","travel");
$msg='';
if(isset($_POST['upload'])){
    $image= $_FILES['image']['name'];
    $path= 'public/imgUser/'.$image;

    $sql = $conn->query("INSERT INTO slider(image_path) VALUES ('$path')");

    if($sql){
        move_uploaded_file($_FILES['image']['tmp_name'],$path);
        $msg = 'Pridaný obrázok úspešne!';
    } else{
        $msg = 'Nepodarilo sa pridať obrázok';
    }
}

$result=$conn->query("SELECT image_path FROM slider");
?>

<br>
<h2>Tu môžeš spolu s ostatnými zdieľať svoje obrázky</h2>
<div class="row justify-content-center">
    <div class="col-lg-4 bg-light rounded px-4">
        <h4 class="text-center text-light p-1">Vyber si obrázok do galérie</h4>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <input type="file" name="image" class="form-control p-1" required>
            </div>
            <div class="form-group">
                <input type="submit" name="upload" class="btn btn-success btn-block" value="Pridaj obrázok">
            </div>
            <div class="form-group">
                <h5 class="text-center text-light"><?= $msg;?></h5>
            </div>
        </form>
    </div>
</div>
<br>
<div class="container-fluid">
    <div class="row justify-content-center mb-2">
        <div class="col-lg-10">
            <div id="demo" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <?php
                    $i=0;
                    foreach ($result as $row){
                        $actives = '';
                        if($i ==0){
                            $actives ='active';
                        }
                        ?>
                        <li data-target="#demo" data-slide-to="<?= $i;?>" class="<?= $actives;?>"></li>
                        <?php $i++; }?>
                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    <?php
                    $i=0;
                    foreach ($result as $row){
                        $actives = '';
                        if($i ==0){
                            $actives ='active';
                        }
                        ?>
                        <div class="carousel-item <?= $actives;?> ">
                            <img src="<?= $row['image_path'];?>" width="90%" height="600">
                        </div>
                        <?php $i++; }?>
                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>
        </div>
    </div>
</div>
