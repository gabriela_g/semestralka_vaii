<?php
/** @var Array $data */
?>
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css"
      rel="stylesheet"  type='text/css'>
<div class="container col-md-10 align-content-center">
    <div class="container mt-3">
        <h2 >Pripomienky</h2>
        <br>
        <a href="?c=Login&a=pridajPripomienku" class="btn btn-info"> Pridaj pripomienku</a>
        <br>

        <?php /** @var /App/Models/Pripomienka $pripomienka */
        foreach ($data['pripomienky']as $pripomienka) { ?>
            <div>
                <br>
                <div class="media border table-dark">
                    <div class="media-body">
                        <p><?= $pripomienka->getText() ?> </p>
                        <div>
                                <a href="?c=Login&a=upravPripomienku&id=<?= $pripomienka->getId() ?>&staryText=<?= $pripomienka->getText() ?>"
                                   class="text-success"title="Upraviť"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="?c=Login&a=vymazPripomienku&id=<?= $pripomienka->getId() ?>" class="text-danger mr-2" title="Vymazať"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

    </div>
</div>


