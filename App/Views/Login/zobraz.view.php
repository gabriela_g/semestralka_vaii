<?php /** @var Array $data */
/** @var \App\Core\Autentifikator $auten */
$auten = new \App\Core\Autentifikator();?>

<?php if ($auten->isAdmin() == 1) {?>
<div class="container">
    <br>
    <h2>Pripomienky používateľov</h2>
    <br>
    <table class="table table-hover table-warning">
        <thead>
        <tr>
            <th>Id</th>
            <th>Pripomienka</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php /** @var \App\Models\Pripomienka $pripomienka1 */
        foreach ($data['pripomienky'] as $pripomienka1) {?>
            <tr>
                <th><?=$pripomienka1->getId()?></th>
                <td><?=$pripomienka1->getText()?></td>
                <td><a href="?c=Login&a=vymazPripomienku&id=<?= $pripomienka1->getID() ?>" class="btn btn-danger">Vymazať</a></td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
<?php }?>