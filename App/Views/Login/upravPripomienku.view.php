<?php /** @var Array $data */?>

<div class="container ">
    <h2>Uprav pripomienku k stránke </h2>
    <form>
        <div class="form-group">
            <label for="comment">Upravená pripomienka:</label>
            <textarea class="form-control" rows="5" id="comment" name="text4" > <?=$data['oldText']?></textarea>
        </div>
        <button formmethod="post" type="submit" class="btn btn-success">Upraviť</button>
    </form>
</div>
