<?php /** @var Array $data */
/** @var App\Core\Autentifikator $autentifikacia */
$autentifikacia = new App\Core\Autentifikator();
?>

<?php if ($autentifikacia->isAdmin() == 1) { ?>
    <div class="container">
        <br>
        <h2>Prihlásení používatelia na stránke</h2>
        <br>
        <table class="table table-bordered table-warning">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
            </tr>
            </thead>
            <tbody>
            <?php /** @var \App\Models\Zakaznik $zakaznik */foreach ($data['zakaznici'] as $zakaznik) { ?>
                <tr>
                    <th><?= $zakaznik->getId() ?></th>
                    <td><?= $zakaznik->getNickname() ?></td>
                    <td><?= $zakaznik->getEmail() ?></td>
                    <td><a href="?c=Login&a=DeleteZakaznik&id=<?= $zakaznik->getID() ?>" class="btn btn-danger">Vymazať</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <?php } else { ?>
    <br>

    <div class="container">
        <h2>Toto sú tvoje údaje </h2>
        <table class="table table-bordered table-info">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th><?= $autentifikacia->loggedInUser()->getId() ?></th>
                <td><?= $autentifikacia->loggedInUser()->getNickname() ?></td>
                <td><?= $autentifikacia->loggedInUser()->getEmail() ?></td>
            </tr>
            </tbody>
        </table>
    </div>
<?php } ?>


