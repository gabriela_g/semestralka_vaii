<br>
<h2>Prihlásenie</h2>
<br>
<div class="row justify-content-center">
    <div class="col-lg-5 rounded bg-light p-3">
        <div class="container mt-5" >
            <form  class="form-horizontal  p-3" >
                <div class="form-group col-md-12">
                    <label class="control-label col-sm-2" for="email"> <strong> Email: </strong>  </label>
                    <div class="col-sm-20">
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <label class="control-label col-sm-2" for="pwd"> <strong> Heslo: </strong> </label>
                    <div class="col-sm-20">
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-5 rounded bg-light p-3">
                        <button formmethod="post" type="submit" name="submit"  class="btn btn-info">Prihlásiť</button>
                    </div>
                    <a class="text-dark" href="?c=Login&a=signup">Ešte nie si zaregistrovaný? Zaregistruj sa tu!</a>
                </div>
            </div>
        <div class="<?=$data['clas']?>" role="<?=$data['role']?>">
            <?=$data['message']?>
        </form>
    </div>
</div>
