<?php
/** @var \App\Core\Autentifikator $auten */
$auten = new \App\Core\Autentifikator();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="https://pixabay.com/get/55e0d6444254b108f5d08460da29317f1537dfe5525576_1280.png">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/css/styl.css">
    <script src="public/script.js"></script>
    <script src="public/mesta.js"></script>

    <title>Travel with us</title>
</head>

<body>
<div class="nav">
    <input type="checkbox" id="nav-check">
    <div class="nav-header">
        <div class="nav-title">
            Travel
        </div>
    </div>
    <div class="nav-btn">
        <label for="nav-check">
            <span></span>
            <span></span>
            <span></span>
        </label>
    </div>
<?php if ($auten->isLoggedIn() == 0) { ?>

        <div class="nav-links">
            <a href="?c=Home" >Domov</a>
            <a href="?c=Home&a=gallery">Galéria</a>
            <a href="?c=Home&a=quiz" >Kvíz</a>
            <a href="?c=Home&a=about" >O nás</a>
            <a href="?c=Home&a=contact" >Kontakt</a>
            <a href="?c=Login" class="btn btn-white text-light bg-dark" type="submit">Prihlásiť</a>
        </div>
    </div>
<?php } else if ($auten->isAdmin() == 1) { ?>
    <div class="nav-links">
        <a href="?c=Home">Domov</a>
        <a href="?c=Login&a=zobraz">Pripomienky</a>
        <a href="?c=Login&a=osobnyUcet">Admin profil</a>
        <a href="?c=Login&a=signout" id="buttonNavbar" class="btn btn-light text-danger" type="submit">Odhlásiť sa</a>

    </div>
</div>
<?php } else { ?>
        <div class="nav-links">
            <a href="?c=Home">Domov</a>
            <a href="?c=Login&a=galleryUser">Galéria</a>
            <a href="?c=Login&a=pripomienky">Pripomienky</a>
            <a href="?c=Login&a=osobnyUcet" >Profil</a>
            <a href="?c=Login&a=signout"  class="btn btn-light text-danger" type="submit">Odhlásiť sa</a>

        </div>
    </div>
<?php } ?>

<div class="web-content">
    <?=  $contentHTML ?>
    </div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>


    <footer id="grid-footer">
        <div class="content-footer">
            <!--<img src="img/logo.png" alt="">-->
            <h4>Travel with us</h4>
            <div>
                <h4>Linky</h4>
                <ul>
                    <li >
                        <a href="?c=Home&a=index" >Domov</a>
                    </li>
                    <li>
                        <a href="?c=Home&a=gallery">Galéria</a>
                    </li>
                    <li>
                        <a href="?c=Home&a=about">O nás</a>
                    </li>
                    <li>
                        <a href="?c=Home&a=contact">Kontakt</a>
                    </li>
                </ul>
            </div>

            <div>
                <h4>Napíšte nám</h4>
                <ul>
                    <li>
                        <a href="https://www.google.sk/maps/place/Bardejov/@49.3044002,21.21153,12z/data=!3m1!4b1!4m5!3m4!1s0x473e80e2e452eb8b:0x400f7d1c6973820!8m2!3d49.2945857!4d21.2754012" target="_blank">Adresa</a>
                    </li>
                    <li>
                        <a href="tel:+421915156211" target="_blank">Telefónne číslo</a>
                    </li>
                    <li>
                        <a href="mailto:travelwithus@gmail.com?Subject=Travel" target="_blank">Email</a>
                    </li>
                </ul>
            </div>

            <div>
                <h4>Newsletter</h4>
                <input type="email" placeholder="zadaj email" class="input-newsletter">
                <button class="submit-newsletter"> Prihlásiť </button>
            </div>
        </div>
    </footer>
</html>