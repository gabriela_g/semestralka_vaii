<?php

namespace App;

use App\Core\Router;

/**
 * Class App
 * Main Application class
 * @package App
 */
class App
{

    /**
     * @var Router
     */
    private $router;

    /**
     * App constructor
     */
    public function __construct()
    {
        $this->router = new Router();
    }

    public function getRouter(): Router
    {
        return $this->router;
    }
    /**
     * Runs the application
     * @throws \Exception
     */
    public function run()
    {

        ob_start();

        $route = $this->router->processURL();

        $data = call_user_func([$route['controller'], $route['action']]);

        require "App" . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . $route['controller']->getName() . DIRECTORY_SEPARATOR . $route['action'] . ".view.php";

        $contentHTML = ob_get_clean();

        require "App" . DIRECTORY_SEPARATOR . "Views" . DIRECTORY_SEPARATOR . "root.layout.view.php";
    }
}
