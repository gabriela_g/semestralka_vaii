<?php

namespace App\Models;

use PDO;

class DBPripomienky
{

    /**
     * @var PDO
     */

    private const DB_HOST = 'localhost';
    private const DB_NAME = 'travel';
    private const DB_USER = 'root';
    private const DB_PASS = 'dtb456';

    private $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:dbname=' . self::DB_NAME . ';host=' . self::DB_HOST, self::DB_USER, self::DB_PASS);
    }

    function loadAllPripomienky() {
        $pripomienky = [];
        $dbPripomienky = $this->db->query('SELECT * FROM pripomienky');
        foreach ($dbPripomienky as $pripomienka)
        {
            $pripomienka2 = new Pripomienka($pripomienka['text']);
            $pripomienka2->setId($pripomienka['id']);
            $pripomienky[] = $pripomienka2;
        }
        return $pripomienky;
    }

    function SavePripomienka(Pripomienka $pripomienka) {
        $pripomienka2=$pripomienka->getText();
        $this->db->query("INSERT INTO pripomienky(text) VALUES ('$pripomienka2')");

    }

    function DeletePripomienka($id) {
        $stmt = $this->db->prepare('DELETE FROM pripomienky where id = ? ' );
        $stmt->execute([$id]);
    }

    function EditPripomienka($id,$newText) {
        $stmt = $this->db->prepare("UPDATE pripomienky SET text=? where id=?");
        $stmt->execute([$newText,$id]);
    }



}