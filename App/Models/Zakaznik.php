<?php

namespace App\Models;

class Zakaznik
{
    private $id;
    private $nickname;
    private $email;
    private $heslo;


    public function __construct($nickname, $email, $heslo, $id=0)
    {
        $this->id = $id;
        $this->nickname = $nickname;
        $this->email = $email;
        $this->heslo = $heslo;
    }

    /**
     * @return mixed
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getHeslo()
    {
        return $this->heslo;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}