<?php

namespace App\Models;
use PDO;

class DBZakaznici
{
    /**
     * @var PDO
     */

    private const DB_HOST = 'localhost';
    private const DB_NAME = 'travel';
    private const DB_USER = 'root';
    private const DB_PASS = 'dtb456';

    private $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:dbname=' . self::DB_NAME . ';host=' . self::DB_HOST, self::DB_USER, self::DB_PASS);
    }

    function loadAll() {
        $zakaznici = [];
        $dbZakaznici = $this->db->query('SELECT * FROM zakaznici');
        foreach ($dbZakaznici as $zakaznik)
        {
            $zakaznik2 = new Zakaznik($zakaznik['nickname'],$zakaznik['mail'], $zakaznik['heslo']);
            $zakaznik2->setId($zakaznik['id']);
            $zakaznici[] = $zakaznik2;
        }
        return $zakaznici;
    }

    function SaveZakaznik(Zakaznik $zakaznik) {
        $nickname22=$zakaznik->getNickname();
        $mail22=$zakaznik->getEmail();
        $heslo22=password_hash($zakaznik->getHeslo(),PASSWORD_DEFAULT);
        $this->db->query("INSERT INTO zakaznici(nickname,mail,heslo) VALUES ('$nickname22','$mail22','$heslo22')");
    }

    function DeleteZakaznik($id) {
        $stmt = $this->db->prepare('DELETE FROM zakaznici where id = ? ' );
        $stmt->execute([$id]);
    }

    function JeToUzivatel($email, $heslo) {
        $statement = $this->db->query("SELECT heslo FROM zakaznici WHERE mail = '$email'");
        $result = $statement->fetch();
        if (($result[0] != "") && password_verify($heslo,$result[0])) {
            return 1;
        } else {
            return 0;
        }
    }

    function DajUzivatela($mail) {
        $statement = $this->db->query("SELECT * FROM zakaznici WHERE mail = '$mail'");
        $result = $statement->fetch();
        return new Zakaznik($result["nickname"],$result["mail"],$result["heslo"],$result["id"]);
    }


}