<?php

namespace App\Models;


class Pripomienka
{
    protected $id;
    private $text;

    public function __construct($text,$id=0)
    {
        $this->id = $id;
        $this->text = $text;

    }

    /**
     * @return int|mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = $text;
    }


}