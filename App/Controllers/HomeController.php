<?php

namespace App\Controllers;

use App\Core\AControllerBase;

class HomeController extends AControllerBase
{

    public function index(){}
    public function about(){}
    public function contact(){}
    public function gallery(){}
    public function quiz(){}


}