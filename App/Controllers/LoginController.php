<?php


namespace App\Controllers;

use App\Core\AControllerBase;
use App\Models\DBZakaznici;
use App\Core\Autentifikator;
use App\Models\Zakaznik;
use App\Models\DBPripomienky;
use App\Models\Pripomienka;


class LoginController extends AControllerBase
{
    public function index()  {
        $storage = new DBZakaznici();
        if (isset($_POST["submit"]) && ($storage->JeToUzivatel($_POST["email"], $_POST["password"]) == 1)) {
            $this->prihlas($storage->DajUzivatela($_POST["email"]));
            header("Location: ?c=Home");
        }
        if(isset($_POST["submit"]) && ($storage->JeToUzivatel($_POST["email"], $_POST["password"]) == 0)) {
            return [
                'message' => "Zadali ste nesprávne heslo! Skúste to znovu.",
                'clas' => "alert alert-danger",
                'role' => "alert"
            ];
        }
        return [
            'message' => "",
            'clas' => "alert alert-white",
            'role' => "alert"
        ];
    }

    public function signup() {
        if (isset($_POST["nickname1"]) || isset($_POST["email1"]) || isset($_POST["heslo1"])) {
            if ($_POST["nickname1"] == "" || $_POST["email1"] == "" || $_POST["heslo1"] == "") { ?>
                <div class="container">
                    <div class="alert alert-danger">
                        <strong>Pozor!</strong> Všetky polia musia byť vyplnené!
                    </div>
                </div>
            <?php }

            if (($_POST["nickname1"]) != "" && ($_POST["email1"]) != "" && ($_POST["heslo1"]) != "") {
                $storage = new DBZakaznici();
                $zakaznik1 = new Zakaznik($_POST["nickname1"],$_POST["email1"], $_POST["heslo1"]);
                $storage->SaveZakaznik($zakaznik1);
                header("Location: ?c=Login&a=index");
            }
        }
    }

    public function signout() {
        $autentifikacia = new Autentifikator();
        $autentifikacia->logoutUser();
    }

    public function prihlas(Zakaznik $zakaznik) {
        $autentifikacia = new Autentifikator();
        $autentifikacia->loginUser($zakaznik);
        header("Location: ?c=Login&a=osobnyUcet");
    }

    public function zakaznici() {
        $storage = new DBZakaznici();
        return ['zakaznici' => $storage->loadAll()];
    }

    public function DeleteZakaznik() {
        if (isset($_GET["id"])) {
            $zakaznici = new DBZakaznici();
            $zakaznici->DeleteZakaznik($_GET["id"]);
            unset($_POST["id"]);
            header("Location: ?c=Login&a=osobnyUcet");
        }
    }

    public function osobnyUcet() {
        $auten = new Autentifikator();
        if ($auten->isLoggedIn() == 0) {
            header("Location: ?c=Login&a=index");
        }
        if($auten->isAdmin() == 1 ) {
            $storage = new DBZakaznici();
            return [ 'zakaznici' => $storage->loadAll()];
        }
    }

    public function galleryUser(){
        $auten = new Autentifikator();
        if ($auten->isLoggedIn() == 0) {
            header("Location: ?c=Login&a=index");
        }

    }

    public function zobraz(){
        $storage = new DBPripomienky();
        return [ 'pripomienky'=> $storage->loadAllPripomienky() ];
        $auten = new Autentifikator();
        if ($auten->isLoggedIn() == 0) {
            header("Location: ?c=Login");
        }
    }

    public function vymazPripomienku() {
        $auten = new Autentifikator();
        if ($auten->isLoggedIn() == 1) {
            if (isset($_GET["id"])) {
                $pripomienky = new DBPripomienky();
                $pripomienky->DeletePripomienka($_GET["id"]);
                unset($_POST["id"]);
                header("Location: ?c=Login&a=pripomienky");
            }
        } if ($auten->isAdmin() == 1) {
            if (isset($_GET["id"])) {
                $pripomienky = new DBPripomienky();
                $pripomienky->DeletePripomienka($_GET["id"]);
                unset($_POST["id"]);
                header("Location: ?c=Login&a=zobraz");
            }
        }
    }

    public function pripomienky(){
        $auten = new Autentifikator();
        if ($auten->isLoggedIn() == 1) {
            $storage = new DBPripomienky();
            return [
                'pripomienky' => $storage->loadAllPripomienky()
            ];
        } else {
            $auten = new Autentifikator();
            header("Location: ?c=Login&a=index");
        }
    }


    public function pridajPripomienku()
    {
        if (isset($_POST["text3"])) {
            if ($_POST["text3"] == "") { ?>
                <div class="container">
                    <div class="alert alert-danger">
                        <strong>Pozor!</strong> Nenapísali ste žiaden text!
                    </div>
                </div>
            <?php }
            if (($_POST["text3"]) != "") {
                $storage = new DBPripomienky();
                $pripomienka = new Pripomienka($_POST["text3"]);
                $storage->SavePripomienka($pripomienka);
                header("Location: ?c=Login&a=pripomienky");
            }
        }
        $auten = new Autentifikator();
        if ($auten->isLoggedIn() == 0) {
            header("Location: ?c=Login&a=index");
        }
    }


    public function upravPripomienku() {
        $auten = new Autentifikator();
        if ($auten->isLoggedIn() == 0) {
            header("Location: ?c=Login&a=index");
        }
        $storage = new DBPripomienky();

            if (isset($_POST["text4"])) {
                if ($_POST["text4"] == "") { ?>
                    <div class="container">
                        <div class="alert alert-danger">
                            <strong>POZOR!</strong> Nič ste nenapísali!
                        </div>
                    </div>
                <?php }
                if ($_POST["text4"] != "") {
                    $pripomienka = new Pripomienka($_POST["text4"]);
                    $storage->EditPripomienka($_GET["id"],$pripomienka->getText());
                    header("Location: ?c=Login&a=pripomienky");
                }
            }

        return [
                'oldText' => $_GET["staryText"]
            ];
    }

}